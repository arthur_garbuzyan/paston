
var playersData,
    trumpAndPlayer,
    players,
    startOfTrump,
    beginningPlayer = 0,
    getHandResults;

/**
 * start new hand
 */
function newHand() {

    clearContent();
    var allCards = shuffle(deck());
    var usersCard = dealer(allCards);
    playersData = new player(usersCard);
    trumpAndPlayer = new playersData.getDominantTrump();
    players = playersData.getPlayers();
    startOfTrump = new GettingTrumps(players, beginningPlayer);
    getHandResults = new startHand(players);
    ((beginningPlayer === startOfTrump.lastKey) ? beginningPlayer = 0 : beginningPlayer++);
}

newHand();
