/**
 * Trumps
 * @param trumpsOption
 * @returns {string}
 */
var createTrumps = function (trumpsOption) {

    var content = '<div id="allTrumps">';
    for (var i in trumpsOption) {

        if (trumpsOption[i] !== 'pass' && trumpsOption[i] !== 'Take') {
            content +=
                '<div class="btn-group trumps" style="margin-left: 4px">' +
                '<button type="button" class="btn trumpBox ' + trumpsOption[i] + '" data-trump="' + trumpsOption[i] + '">' +
                '<img src="img/' + trumpsOption[i] + '.png" alt="" class="' + trumpsOption[i]+'-card spadeBtn" width="40">' +
                '</button>' +
                '</div>';
        } else {
            content +=
                '<div class="btn-group trumps" >' +
                '<button type="button" class="btn trumpBox passBtn pass" style="padding-top: 21%; padding-bottom: 21%" data-trump="pass">' +
                trumpsOption[i] +
                '</button>' +
                '</div>';
        }
    }
    content += '</div>';
    return content;
};

/**
 * Right hands content
 * @param players
 */
var createHandCountBox = function (players) {

    var handsCursorBox = document.getElementById('handBlock'),
        cursorElements = '<p class="borderBottom"><b>Hands count</b></p>';
    for (var i in players) {
        cursorElements += '<div class="'+players[i].position+'Place"><b>'+players[i].name+'</b> <br> <span>'+players[i].hands+'</span></div>'
    }
    handsCursorBox.innerHTML = cursorElements;
};

var centerCardsContent = document.getElementById('centerBlock');
var centerCards = function (cardName) {

    centerCardsContent.innerHTML += '<img width="70" src="img/'+cardName+'.png" class="block h-center v-center card disabledCards" alt="">';
};

/**
 * players card
 * @param cards
 * @param position
 * @param name
 */
var createCards = function (cards, position, name) {

    var getPosition, content = '', card = '';
    for (var i in  cards) {
        ((name === 'bot')?cards[i].imgName = 'botCard':card = 'card');
        content +='<img width="70" src="img/' + cards[i].imgName + ".png" + '" data-name="' + cards[i].name + '" data-trump="' + cards[i].trump + '" class="card disabledCards" alt="">';
    }
    getPosition = document.getElementById(position);
    getPosition.insertAdjacentHTML('beforeend', content);
};

/**
 * Left scores content
 * @param players
 * @param scores
 */
var gameScoreBox = function (players, scores) {

    var handsCursorBox = document.getElementById('scoreBlock'),
        cursorElements = '<p class="borderBottom"><b>Game score</b></p>';
        handsCursorBox.classList.remove('hidden');
    for (var i in players) {
        cursorElements += '<div class="'+players[i].position+'PlayerScore"><b>'+players[i].name+'</b> <br> <span>'+scores[i].playerScore+'</span></div>'
    }
    handsCursorBox.innerHTML = cursorElements;
};

/**
 *
 * @param players
 * @param scores
 */
var modalAction = function (players, scores) {

    var modalBody = document.querySelectorAll('.modal-body')[0];
    var overlay = document.querySelectorAll('.modal-overlay')[0];
    var whichModal = document.querySelectorAll('#lil-bit-content')[0];
    var modalUsersData = '<img src="img/'+trumpAndPlayer.trump+'.png" width="40">';

    for (var i in players) {
        modalUsersData += '<p><b>'+players[i].name+'</b>-  '+scores[i].playerScore+'</p>'
    }
    modalBody.innerHTML = modalUsersData;
    overlay.classList.toggle('modal-is-visible');
    whichModal.classList.toggle('modal-is-visible');
};

var closeModal  = function(){
    var overlay = document.querySelectorAll('.modal-overlay')[0];
    var whichModal = document.querySelectorAll('#lil-bit-content')[0];
    overlay.classList.toggle('modal-is-visible');
    whichModal.classList.toggle('modal-is-visible');
};