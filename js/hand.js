function getGameWinner(gamePlayers, trumpAndPlayer) {

    var getCalculateData = calculatingGameScores( gamePlayers, trumpAndPlayer.player.trump);
    gameScoreBox(gamePlayers, getCalculateData);
    getClicks('close-modal-trigger', closeModal);
    modalAction(gamePlayers, getCalculateData);
}

function startHand(player) {

    var lastVal = (player[player.length - 1]),
        lastKey = player.indexOf(lastVal),
        playersCount = player.length,
        playingCards = [],
        handWinners = [],
        maxHandsCount = 10,
        _self = this;
    this.player = player;
    this.getUserScore = function () {
        if (handWinners.length === maxHandsCount) {
            getGameWinner(_self.player, trumpAndPlayer);
            newHand();
        } else {
            return true;
        }
    };

    this.handEndResult = function () {

        var handWinner = [];
        if (playingCards.length === playersCount) {
            handWinner = handPlayer(playingCards, trumpAndPlayer);
            handWinners.push(handWinner);
            playersData.hand(handWinner, player);
            playingCards = [];
            return handWinner.userId;

        } else {
            return null;
        }
    };

    this.start = function (startPlayer) {

        for (var e in player) {
            if (player[e].trump === 'exit') {
                --playersCount;
            }
        }
        this.id = startPlayer;
        enableOrDisableCards(player[this.id].position, false);
        var key = this.id;

        function moveCard() {

            _self.centerActions(this);
        }

        function changePlayer() {

            enableOrDisableCards(_self.player[key].position, true);
            ((_self.player[key] === _self.player[lastKey]) ? key = 0 : key++);
            ((_self.player[key].trump === 'exit') ? key++ : key);
            var handGetter = _self.handEndResult(trumpAndPlayer.trump);
            if (handGetter === null) {
                enableOrDisableCards(_self.player[key].position, false);
            } else {
                key = handGetter;
                setTimeout(function () {
                    centerCardsContent.innerHTML = '';
                    enableOrDisableCards(_self.player[key].position, !_self.getUserScore() || false);
                }, 1500);
            }
        }

        this.centerActions = function (thisElement) {

            var getActualTrump, firstMove, hasPlayerCard, hasPlayerTrump, thisCardName, findCardObj, currentActionObj;
            thisCardName = thisElement.getAttribute('data-name');
            findCardObj = searchObjVal([thisCardName], player[key].cards, 'name');
            currentActionObj = {
                userId: key,
                card: findCardObj,
                cardValue: findCardObj.value,
                trump: findCardObj.trump,
                hasActualTrump: ''
            };
            function roundAction() {
                centerCards(thisCardName);
                playingCards.push(currentActionObj);
                removeDomCard(thisElement);
                removeCard(key, findCardObj.name);
                changePlayer();
            }
            if (!playingCards.length) {
                roundAction();
            } else {
                getActualTrump = trumpAndPlayer.trump;
                firstMove = playingCards[0].card.trump;
                hasPlayerCard = searchObjVal([firstMove], players[currentActionObj.userId].cards, 'trump');
                hasPlayerTrump = !!searchObjVal([getActualTrump], players[currentActionObj.userId].cards, 'trump');

                if (hasPlayerCard) {
                    if (currentActionObj.card.trump === firstMove) {roundAction();}
                } else if (hasPlayerTrump) {
                    if (currentActionObj.trump === trumpAndPlayer.trump) {roundAction();}
                }
                else {
                    currentActionObj.hasActualTrump = null;
                    roundAction();
                }
            }
        };
        getClicks('card', moveCard);
    };
}
