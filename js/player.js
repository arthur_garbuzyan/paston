var player = function (allCards) {

    this.playerData = function (position, name, cards) {

        this.id = '';
        this.cards = cards;
        this.position = position;
        this.name = name;
        this.hands = 0;
        this.score = 0;
        this.chooseTrump = false;
        this.trump = null;
        createCards(cards,position,name);
    };

    this.getPlayers = function () {

      return [
          new this.playerData('left', 'John', allCards.getPlayerOneCards),
          new this.playerData('right', 'Sara', allCards.getPlayerTwoCards),
          new this.playerData('bottom', 'Tom', allCards.getPlayerThreeCards)
      ]
    };

    this.getDominantTrump = function () {

        this.trump = false;
        this.player = {};
    };

    this.hand = function (handGetterData, players) {

        players[handGetterData.userId].hands++;
        document.querySelectorAll('.' + players[handGetterData.userId].position + 'Place span')[0].innerHTML = players[handGetterData.userId].hands;
    }
};