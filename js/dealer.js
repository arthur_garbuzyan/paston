
/**
 * Create cards
 * @param value
 * @param rank
 * @param trump
 */
function card(value, rank, trump) {
    this.rank = rank;
    this.value = value;
    this.trump = trump;
    this.name = this.rank + "of" + this.trump;
    this.imgName = this.rank + "of" + this.trump;
}

function deck() {
    this.rank = ['7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    this.trump = ['hearts', 'diamonds', 'spades', 'clubs'];
    var cards = [];
    var trumpLength = this.trump.length;
    var rankLength = this.rank.length;

    for (var s = 0; s < trumpLength; s++) {
        for (var n = 0; n < rankLength; n++) {
            cards.push(new card(n + 1, this.rank[n], this.trump[s]));
        }
    }
    return cards;
}

/**
 * Shuffle all cards
 * @param sourceArray
 * @returns {*}
 */
function shuffle(sourceArray) {
    for (var i = 0; i < sourceArray.length - 1; i++) {
        var j = i + Math.floor(Math.random() * (sourceArray.length - i));
        var temp = sourceArray[j];
        sourceArray[j] = sourceArray[i];
        sourceArray[i] = temp;
    }
    return sourceArray;
}

/**
 * Add cards in hand.js
 * @param shuffleCards
 * @returns {{getPlayerOneCards: (Array.<T>|string|Blob|ArrayBuffer), getPlayerTwoCards: (Array.<T>|string|Blob|ArrayBuffer), getPlayerThreeCards: (Array.<T>|string|Blob|ArrayBuffer), additionalTwoCards: (Array.<T>|string|Blob|ArrayBuffer)}}
 */
function dealer(shuffleCards) {
    return {
        getPlayerOneCards: shuffleCards.slice(0, 10),
        getPlayerTwoCards: shuffleCards.slice(10, 20),
        getPlayerThreeCards: shuffleCards.slice(20, 30),
        additionalTwoCards: shuffleCards.slice(30, 32)
    }
}

var allCards = shuffle(deck());
var  usersCard = dealer(allCards);

