/**
 *
 * @param keyName
 * @param objectName
 * @param objectKey
 * @returns {*}
 */
function searchObjVal(keyName, objectName, objectKey) {

    var result = null,
        nameCount = objectName.length,
        keyCount = keyName.length;
    for (var i = 0; i < nameCount; i++) {
        for (var v = 0; v < keyCount; v++) {
            if (!objectName[i].hasOwnProperty('name')) {
                continue
            }
            switch (objectKey) {
                case 'name':
                    if (objectName[i].name === keyName[v]) {
                        result = objectName[i];
                    }
                    break;
                case 'trump':
                    if (objectName[i].trump === keyName[v]) {
                        result = objectName[i];
                    }
                    break;
                case 'value':
                    if (objectName[i].value === keyName[v]) {
                        result = objectName[i];
                    }
                    break;
            }
        }
    }
    return result;
}

/**
 *
 * @param cardContentId
 */
function enableOrDisableCards(cardContentId, addOrRemove) {

    var getContentElements = document.getElementById(cardContentId).getElementsByClassName("card"),
        arrowForTurn = document.getElementsByClassName('arrow-' + cardContentId),
        countOfContentElements = getContentElements.length;

    for (var i = 0; i < countOfContentElements; i++) {
        if (!addOrRemove) {
            getContentElements[i].classList.remove('disabledCards');
        } else {
            getContentElements[i].classList.add('disabledCards')
        }
    }
    ((!addOrRemove) ? arrowForTurn[0].classList.add('displayBlock') : arrowForTurn[0].classList.remove('displayBlock'));
}

/**
 *
 * @param id
 */
function removeIdContent(id) {
    var content = document.getElementById(id);
    ((content) ? content.innerHTML = '' : '');
}

/**
 *
 * @param position
 * @param htmlElements
 */
function setTrumpPosition(position, htmlElements) {

    var contentForTrump = document.getElementById(position + 'TrumpsContent');
    contentForTrump.insertAdjacentHTML('beforeend', htmlElements);
}

/**
 *
 * @param className
 * @param action
 */
function getClicks(className, action) {

    var trumpBox = document.getElementsByClassName(className),
        length = trumpBox.length;
    for (var i = 0; i < length; i++) {
        trumpBox[i].addEventListener("click", action);
    }
}

/**
 *
 * @param className
 * @param trueOrFalse
 */
function disableOrEnable(className, trueOrFalse) {

    var trumpBox = document.getElementsByClassName(className),
        length = trumpBox.length;
    for (var i = 0; i < length; i++) {
        ((trueOrFalse) ? trumpBox[i].disabled = false : trumpBox[i].disabled = true);
    }
}

/**
 *
 * @param trumps
 * @param trump
 */
function offWeakTrumps(trumps, trump) {

    var getKey = trumps.indexOf(trump);

    for (var i in trumps) {
        if (trumps[i] === 'pass') {
            continue
        }
        if (trumps[getKey] === 'pass') {
            break
        }
        disableOrEnable(trumps[i], false);
        if (trumps[i] === trumps[getKey]) {
            break;
        }
    }
}

/**
 *
 * @param playerTrump
 * @param count
 * @returns {*}
 */
function passCount(playerTrump, count) {

    var passCount = [];
    for (var i in playerTrump) {
        if (playerTrump[i].hasOwnProperty('trump')) {
            ((playerTrump[i].trump === 'pass') ? passCount.push(playerTrump[i].trump) : '' );
        }
    }

    return ((passCount.length === count) ? true : '');
}

/**
 *
 * @param status
 * @param position
 */
function necessaryTrumps(status, position) {

    removeIdContent('allTrumps');
    var additional = ['spades', 'clubs', 'diamonds', 'hearts', 'exit'],
        passOrExit = ['Take', 'cutting', 'exit'],
        exit = ['exit'];

    switch (status) {
        case 'additional':
            var getAdditional = createTrumps(additional);

            setTrumpPosition(position, getAdditional);
            var exitBtn = document.getElementsByClassName('exit')[0];
            exitBtn.setAttribute('data-runAgain', 'runGameAgain');
            break;
        case 'passOrExit':
            var getPassOrExit = createTrumps(passOrExit);
            setTrumpPosition(position, getPassOrExit);
            break;
        case 'exit':
            var getExit = createTrumps(passOrExit);
            setTrumpPosition(position, getExit);
            break;
    }
}

/**
 *
 * @param status
 * @param currentPlayer
 * @param beginningPlayer
 * @param thisHtmlElement
 */
function exitTrumpCircle(status, currentPlayer, beginningPlayer, thisHtmlElement) {

    var getAllTrumps = trumpOptions, playerWithTrump, handStartPlayer;
    getAllTrumps.pop();

    function getPlayerWithTrump() {
        for (var x = getAllTrumps.length; x > 0;) {
            x--;
            for (var n in players) {
                if (players[n].trump === getAllTrumps[x]) {
                    playerWithTrump = players[n];
                    return playerWithTrump;
                }
            }
        }
    }

    handStartPlayer = getPlayerWithTrump();

    switch (status) {
        case 'additional':
            var trump = thisHtmlElement.getAttribute('data-trump');
            currentPlayer.trump = trump;
            trumpAndPlayer.player = currentPlayer;
            if (currentPlayer.trump === 'exit') {
                getGameWinner(players, trumpAndPlayer);
                newHand();
            } else {
                getHandResults.start(beginningPlayer);
                trumpAndPlayer.trump = currentPlayer.trump;
                trumpAndPlayer.player = currentPlayer;
                removeIdContent('allTrumps');
            }
            break;
        case 'cutting' :
            getHandResults.start(beginningPlayer);
            trumpAndPlayer.player = currentPlayer;
            currentPlayer.trump = 'cutting';
            trumpAndPlayer.trump = 'spades';
            removeIdContent('allTrumps');
            break;
        case 'exit':
            getHandResults.start(handStartPlayer.id);
            trumpAndPlayer.player = handStartPlayer;
            trumpAndPlayer.trump = handStartPlayer.trump;
            removeIdContent('allTrumps');
            break;
        case 'pass':
            getHandResults.start(beginningPlayer);
            trumpAndPlayer.player = handStartPlayer;
            trumpAndPlayer.trump = handStartPlayer.trump;
            removeIdContent('allTrumps');
            break;
        case 'clear':
            getHandResults.start(beginningPlayer);
            enableOrDisableCards(currentPlayer.position);
            trumpAndPlayer.player = currentPlayer;
            trumpAndPlayer.trump = currentPlayer.trump;
            removeIdContent('allTrumps');
    }
}

/**
 *
 * @param trumpContent
 * @param position
 */
function moveTrumps(trumpContent, position) {

    if (trumpContent) {
        var fragment = document.createDocumentFragment();
        fragment.appendChild(trumpContent);
        document.getElementById(position + 'TrumpsContent').appendChild(fragment);
    } else {
        alert('Id content not found');
    }
}

/**
 *
 * @param counter
 * @param clickThis
 * @param key
 * @returns {*}
 */
function removeAdditionalCards(counter, clickThis, key) {

    var getCard;
    getCard = clickThis.getAttribute('data-name');
    if (counter <= 2) {
        clickThis.parentNode.removeChild(clickThis);
        removeCard(key, getCard);
        return ((counter === 2) ? true : '');
    }
}

/**
 *
 * @param playerKey
 * @param cardName
 */
function removeCard(playerKey, cardName) {

    for (var i in players[playerKey].cards) {
        if (players[playerKey].cards[i].name === cardName) {
            players[playerKey].cards[i] = {};
        }
    }
}

function removeDomCard(thisClick) {
    thisClick.parentNode.removeChild(thisClick);
}

/**
 *
 * @param inputArray
 * @param trump
 * @returns {Array}
 */
function hasPlayingTrump(inputArray, trump) {

    var centerActionTrumps = [];

    for (var i in inputArray) {
        if (inputArray[i].trump === trump) {
            centerActionTrumps.push(inputArray[i]);
        }
    }

    return centerActionTrumps;
}

/**
 *
 * @param playingCards
 * @param trumpAndPlayer
 * @returns {*}
 */
function handPlayer(playingCards, trumpAndPlayer) {

    var getUsingTrumpCount = hasPlayingTrump(playingCards, trumpAndPlayer);
    var withOutTrumps, haveOneTrump, haveSomeTrumps;

    if (!getUsingTrumpCount.length) {
        withOutTrumps = playingCards.reduce(function (prev, current) {
            if (current.hasActualTrump === null) {
                return withOutTrumps = playingCards[0];
            }
            return (prev.cardValue > current.cardValue) ? prev : current
        });
        return withOutTrumps;
    } else if (getUsingTrumpCount.length > 1) {
        haveSomeTrumps = getUsingTrumpCount.reduce(function (prev, current) {
            return (prev.cardValue > current.cardValue) ? prev : current
        });
        return haveSomeTrumps;
    } else {
        haveOneTrump = getUsingTrumpCount[0];
        return haveOneTrump;
    }
}

/**
 * Calculating scores
 * @param players
 * @param activeTrump
 * @returns {Array}
 */
function calculatingGameScores(players, activeTrump) {
    var lastScore = [];
    var coefficient = '';
    switch (activeTrump) {
        case 'cutting':
            coefficient = 1;
            break;
        case 'spades':
            coefficient = 1;
            break;
        case 'clubs':
            coefficient = 2;
            break;
        case 'diamonds':
            coefficient = 3;
            break;
        case 'hearts':
            coefficient = 4;
            break;
        case 'exit':
            for (var i in players) {
                if (i != trumpAndPlayer.player.id) {
                    lastScore[i] = {
                        playerScore: 0,
                        player: players[i]
                    }
                } else {
                    lastScore[i] = {
                        playerScore: -10,
                        player: players[i]
                    }
                }
            }
            return lastScore;
    }


    for (var i in players) {

        if (i != trumpAndPlayer.player.id) {

            if(players[i].trump === 'exit'){lastScore[i]={playerScore: 0}; continue;}
            if (players[i].hands < 3) {
                lastScore[i] = {
                    playerScore: -10 * coefficient,
                    player: players[i]
                }
            } else {
                lastScore[i] = {
                    playerScore: players[i].hands * coefficient,
                    player: players[i]
                }
            }
        } else {

            var trumpPlayerCoefficient = ((activeTrump === 'cutting') ? 4 : coefficient);
            if (players[i].hands < 6) {
                lastScore[i] = {
                    playerScore: -10 * trumpPlayerCoefficient,
                    player: players[i]
                }
            } else {
                lastScore[i] = {
                    playerScore: players[i].hands * trumpPlayerCoefficient,
                    player: players[i]
                }
            }
        }
    }

    return lastScore;
}

function removeDomArrow() {
    var arrowClass1 = document.getElementById('arrow0');
    var arrowClass2 = document.getElementById('arrow1');
    var arrowClass3 = document.getElementById('arrow2');

    arrowClass1.classList.remove("displayBlock");
    arrowClass2.classList.remove("displayBlock");
    arrowClass3.classList.remove("displayBlock");
}

function clearContent() {

    removeIdContent('right');
    removeIdContent('left');
    removeIdContent('bottom');
    removeIdContent('bottomTrumpsContent');
    removeIdContent('leftTrumpsContent');
    removeIdContent('rightTrumpsContent');
    removeDomArrow();
    centerCardsContent.innerHTML = '';
}