
var trumpOptions = ['pass', 'clubs', 'diamonds', 'hearts', 'cutting'];

/**
 *
 * @param player
 * @param beginningPlayerKey
 * @constructor
 */
function GettingTrumps(player, beginningPlayerKey) {

    var _this = this,
        trumpOptions = ['pass', 'clubs', 'diamonds', 'hearts', 'cutting'],
        trumpHtmlElements = createTrumps(trumpOptions),
        isDelAdditionalCards,
        e = 0,
        x = 0;

    this.lastVal = (player[player.length - 1]);
    this.lastKey = player.indexOf(this.lastVal);
    createHandCountBox(players);

    setTrumpPosition(player[beginningPlayerKey].position, trumpHtmlElements);
    var trumpContent = document.getElementById('allTrumps');

    function additionalAction() {

        e += 1;
        isDelAdditionalCards = removeAdditionalCards(e, this, _this.key);
        if (isDelAdditionalCards) {

            getClicks('trumpBox', function () {
                exitTrumpCircle('additional', player[_this.key], beginningPlayerKey, this);
            });
            disableOrEnable('trumpBox', true);
        }
    }

    this.trumpsSecondCircle = function () {

        x++;
        if (x === player.length) {

            var getAdditionalCard = passCount(player, player.length),
                adoptedTrump = passCount(player, player.length - 1),
                beginningPlayer = player[beginningPlayerKey],
                starterTrump = player[beginningPlayerKey].trump,
                beginningTrumpKey = trumpOptions.indexOf(starterTrump);
                beginningPlayer.id = beginningPlayerKey;

            if (getAdditionalCard) {

                alert('additional');
                necessaryTrumps('additional', beginningPlayer.position);
                disableOrEnable('trumpBox', false);
                addAdditionalCards();
                getClicks('card', additionalAction);
                enableOrDisableCards(beginningPlayer.position);
            }

            else if (adoptedTrump) {
                if (beginningPlayer.trump !== 'pass') {
                    exitTrumpCircle('clear', beginningPlayer, beginningPlayerKey)
                } else {
                    alert('your trump value is little. pass or exit');
                    getClicks('trumps', function () {
                        beginningPlayer.trump = this.getAttribute('data-trump');
                        exitTrumpCircle(beginningPlayer.trump, beginningPlayer);
                    });
                }
            }

            for (var i = 0; i < player.length; i++) {
                if (beginningTrumpKey < trumpOptions.indexOf(player[i].trump)) {
                    necessaryTrumps('passOrExit', beginningPlayer.position);
                    getClicks('trumpBox', function () {
                        beginningPlayer.trump = this.getAttribute('data-trump');
                        exitTrumpCircle(beginningPlayer.trump, player[_this.key].id, beginningPlayer.id);
                    });
                    break;
                }
            }
        }
    };

    function trumpClick() {

        var thisTrump = this.getAttribute("data-trump");
        offWeakTrumps(trumpOptions, thisTrump);
        player[_this.key].trump = thisTrump;
        player[_this.key].id = _this.key;
        if (thisTrump === 'cutting') {
            return exitTrumpCircle('cutting', player[_this.key], beginningPlayerKey);
        }
        if (thisTrump === 'exit') {
            return exitTrumpCircle('exit', player[_this.key], beginningPlayerKey);
        }
        _this.chooseTrumpAndRun(player);
    }

    function addAdditionalCards() {
        var getPlayer = _this.player[beginningPlayerKey],
            additionalCards = usersCard.additionalTwoCards;
        createCards(additionalCards, getPlayer.position, getPlayer.name);
        getPlayer.cards.push(additionalCards[0], additionalCards[1]);
    }

    this.key = beginningPlayerKey;
    this.player = player;
    this.chooseTrumpAndRun = function (player) {

        _this.trumpsSecondCircle(player);

        ((player[_this.key] === player[_this.lastKey]) ? _this.key = 0 : _this.key++);
        var position = player[_this.key].position, currentPLayer = player[_this.key];
        moveTrumps(trumpContent, position);

        ((currentPLayer.name === 'bot' && !currentPLayer.trump) ? _this.changeTrump(player) : '');
    };

    this.changeTrump = function (player) {

        if (player[_this.key].name === 'bot') {
            player[_this.key].trump = 'pass';
            _this.chooseTrumpAndRun(player);
        }

        player[_this.key].chooseTrump = true;
    };
    getClicks('trumpBox', trumpClick);
}


